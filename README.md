**Purpose**

* Provide a web portal for students to register for when they will present their
projects from a provided list of times. If a student trys to register again they
will be informed that they have already registered and when they registered to
present. Students cannot modify their registration. 

* Allows the professor to set the times (and location) as well as limit how many
students can sign up for each time.

**Dependencies**

* PHP

* SQLite 

**Possible Feature To Add**

* Report page to pull schedule out of the database

* Add Admin functionality to be able to:

  * Edit student registration

  * Edit classes

  * Edit schedules

  * Remove student registrations