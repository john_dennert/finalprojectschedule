<?php
	$databaseFile = "database/finalProjectSchedule.db"; //path to SQLite database file
	
	$data = $_POST['data_to_send']; //get data from POST request
	$operation = $_POST['operation']; //operation to be performed
	
	if ($operation == "listClasses") {
		listClasses($data, $databaseFile);
	} elseif ($operation == "listTimes") {
		listTimes($data, $databaseFile);
	} elseif ($operation == "register") {
		register($data, $databaseFile);
	} elseif ($operation == "generate") {
		generateReport($data, $databaseFile);
	} else { //debugging
		echo("unknown error");
	}//end if
	
	function connectToDB($databaseFile) {
		try {
			$database = new PDO('sqlite:'.$databaseFile);
			return $database;
		} catch(Exception $error) {
			echo("This error occured: ".$error->getMessage());
		} //end catch
	} //end connectToDB
	
	function listClasses($data, $databaseFile) {
		//query database for active classes
		$db = connectToDB($databaseFile);
		
		if (!$db) {
			return;
		} //end if
		
		$sql = "SELECT name FROM class WHERE active=1";
		
		$result = $db->query($sql);
		
		if ($result) {
			$rows = $result->fetchall(PDO::FETCH_ASSOC);
			foreach ($rows as $row) {
				echo($row['name'].",");
			} //end foreach
		} //end if
	} //end listClasses
	
	function listTimes($data, $databaseFile) {
		//return time/capacity based on class passed
		$db = connectToDB($databaseFile);
		if (!$db) {
			return;
		} //end if
		
		$class = rtrim($data['class']['value'],"\n");
		$sql = "SELECT id, day, date, time, location, capacity FROM schedule WHERE class='$class'";
		
		$result = $db->query($sql);
		
		if ($result) {
			$rows = $result->fetchall(PDO::FETCH_ASSOC);
			foreach ($rows as $row) {
				$sql = "SELECT COUNT(schedule_id) AS count FROM studentRegistration WHERE schedule_id='{$row['id']}'";
				$count = $db->query($sql);
				if ($count) {
					$count = $count->fetch(PDO::FETCH_ASSOC);
					$capacity = $row['capacity'] - $count['count'];
					echo($row['day'].", ".$row['date'].", ".$row['time'].",;in ".$row['location']).";".$capacity.";".$row['id']."\n";
				} //end if
			} //end foreach			
		} //end if
	} //end listTimes
	
	function register($data, $databaseFile) {
		//attempt to register student
		//if already registered, tell student when they are registered
		
		//name - student name
		//times - id of schedule record
		
		$name = $data['name']['value']; //get from $data
		$time = rtrim($data['times']['value'], "\n");
		$found = false;

		$db = connectToDB($databaseFile);
		if (!$db) {
			echo("error");
			return;
		} //end if
		
		//check if record($time, $name) already exists in the database
		$sql = "SELECT COUNT(schedule_id) AS count FROM studentRegistration WHERE studentName='$name' AND schedule_id='$time'";
		
		$result = $db->query($sql);
		
		if ($result) {
			$count = $result->fetch(PDO::FETCH_ASSOC);
			if ($count['count'] > 0) {
				$found = true;
			} //end if
		} //end if
		
		if ($found) { //if found lookup info about $time in schedule table
			$sql = "SELECT day, date, time, location FROM schedule WHERE id='$time'";
			$result = $db->query($sql);
				
			if ($result) {
				$data = $result->fetch(PDO::FETCH_ASSOC);
				echo("You are already registered for {$data['day']}, {$data['date']}, at {$data['time']} in {$data['location']}.");
			} //end if
		} else { //else insert record($time, $name) into schedule table
			$sql = "INSERT INTO studentRegistration VALUES ($time, '$name')";
			
			$result = $db->query($sql);
			
			if ($result) { //return information about when they registered
				$sql = "SELECT day, date, time, location FROM schedule WHERE id='$time'";
				$result = $db->query($sql);
				
				if ($result) {
					$data = $result->fetch(PDO::FETCH_ASSOC);
					echo("Registered for {$data['day']}, {$data['date']}, at {$data['time']} in {$data['location']}.");
				} //end if
			} else {
				echo("$sql\n");
				print_r($db->errorInfo());
				echo("error");
			} //end if
		} //end if
	} //end register

	function generateReport($data, $databaseFile) {
		//generate report a given day and time
		$time = $data['times']['value'];
		
		if ($time == "") { //no time selected
			echo("Please select a time.\n");
			return;
		} //end if
		
		$db = connectToDB($databaseFile);
		if (!$db) { //unable to connect to the database
			echo("error");
			return;
		} //end if
		
		$sql = "SELECT studentName FROM studentRegistration WHERE schedule_id='$time'";
		$result = $db->query($sql);
		
		if ($result) {
			$names = $result->fetchall(PDO::FETCH_ASSOC);
			if (count($names) == 0) { //no one has registered for this time
				echo("No one has registered for this time.\n");
			} //end if
			foreach ($names as $name) {
				echo($name['studentName']."\n");
			} //end foreach
		} else {
			echo("error");
		} //end if
	} //end generateReport
?>