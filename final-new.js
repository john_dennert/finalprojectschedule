var information = {};
var request;
var contactInfo = "Justin Copeland, jdcopela@iupui.edu";

function init() {
	loadClasses();
	addContactInfo();
	resetForm();
} //end init

function addContactInfo() {
	var instructions = document.getElementById("instructions");
	instructions.innerHTML = instructions.innerHTML + "You can contact " + contactInfo + ", if you have questions.";
}

function loadClasses() {
	//get class.txt
	$.post("final-new.php", {"data_to_send":"", "operation":"listClasses"}, function(response, status, xhr) {
		//create select option for each class
		var classes = response.split(",");
		var classSelect = document.getElementById("classes");
		
		//make sure select doesn't have any options
		while (classSelect.length != 0) {
			classSelect.remove(0);
		} //end while
		
		classSelect.options[0]=new Option("","");
		for (counter=0; counter<classes.length-1; counter++){ //last element will be an empty string
			classSelect.options[counter+1]=new Option(classes[counter], classes[counter]);
		} //end for
	} //end function
	);
} //end loadClasses

function keydown(event) {
	if (event.keyCode == "13") { //map Return key to save function
		event.preventDefault();
		save();
	} //end if
} //end keydown

function save() {
	//validate form
	var errors = "<p>";
	
	if (document.getElementById("name").value == "") {
		errors += "Please enter your name.<br />";
	} //end if
	
	if (document.getElementById("classes").value == "") {
		errors += "Please select a class.<br />";
	} //end if
	
	if (document.getElementById("times").value == "") {
		errors += "Please select a time.<br />";
	} //end if
	
	if (errors == "<p>") { //if no errors continue submitting form, 
		var divOutput = document.getElementById("errors");
		divOutput.innerHTML = "";
	    //pack up form data
		packageData("form");
		//submit form to PHP, send data using AJAX in JSON format
		$.post("final-new.php", {"data_to_send":information,"operation":"register"}, finished);
    } else { //if error don't submit or reset form, provide feedback to user
	    var divOutput = document.getElementById("errors");
		divOutput.innerHTML = errors + "</p>";
    } //end if
} //end save

function finished(response, status, xhr) {
	//check for success
	if (response.substr(0,10) == "Registered") { //tell user
		alert(response);
		resetForm();
	} else if (response.substr(0,26) == "You are already registered") {
		alert(response+" Please contact " + contactInfo + ", if you need to change.");
		resetForm();
	} else {
		//alert(response);
		alert("An error occurred. Please contact " + contactInfo + ".");
	} //end if
} //end finished

function resetForm() {
	document.getElementById("name").value = ""; //erase name
	timeSelect = document.getElementById("times");
	timeSelect.hidden = "hidden"; //hid times select
	timeLabel = document.getElementById("timesLabel");
	timesLabel.hidden = "hidden";
	loadClasses();
} //end resetForm

function resetReportForm() {
	timeSelect = document.getElementById("times");
	timeSelect.hidden = "hidden"; //hid times select
	timeLabel = document.getElementById("timesLabel");
	timesLabel.hidden = "hidden";
	document.getElementById("report").innerHTML = "";
	loadClasses();
} //end resetReportForm

function loadTimes() {
	//unhid the times select and its label
	var timeSelect = document.getElementById("times");
	timeSelect.hidden = "";
	var label = document.getElementById("timesLabel")
	label.hidden = "";
	
	//make sure select doesn't have any options
	while (timeSelect.length != 0) {
		timeSelect.remove(0);
	} //end while
	
	var className = document.getElementById("classes").value;
	information["class"] = {"value": className};
	//check available times/capacity
	$.post("final-new.php", {"data_to_send":information, "operation":"listTimes"}, function(response, status, xhr) {
		//if capacity == 0 then disable option
		var times = response.split("\n");
		for (counter=0; counter < times.length-1; counter++) { //last element will be empty
			times[counter]=times[counter].split(";");
		} //end foreach
		
		var timeSelect = document.getElementById("times");
		timeSelect.options[0]=new Option("",""); //first option is blank
		for (counter=0; counter<times.length-1; counter++){ //last element will be empty
			timeSelect.options[counter+1]=new Option(times[counter][0]+" "+times[counter][1]+" ("+times[counter][2]+" spots left)", times[counter][3]);
			if (times[counter][2] < 1) {
				timeSelect.options[counter+1].disabled = "disabled";
			} //end if
		} //end for
	} //end function
	);
} //end loadTimes

function packageData(formName) {
	var formElements = document.getElementById(formName).elements;
	
	for(counter = 0; counter < formElements.length; counter++) {
		information[formElements[counter].name] = {"value":formElements[counter].value};
	} //end for
} //end packageData

function reportLoadTimes() {
	var timeSelect = document.getElementById("times");
	timeSelect.hidden = "";
	var label = document.getElementById("timesLabel")
	label.hidden = "";
	var report = document.getElementById("report");
	report.innerHTML = "";
	
	//make sure select doesn't have any options
	while (timeSelect.length != 0) {
		timeSelect.remove(0);
	} //end while
	
	var className = document.getElementById("classes").value;
	information["class"] = {"value": className};
	//check available times/capacity
	$.post("final-new.php", {"data_to_send":information, "operation":"listTimes"}, function(response, status, xhr) {
		//if capacity == 0 then disable option
		var times = response.split("\n");
		for (counter=0; counter < times.length-1; counter++) { //last element will be empty
			times[counter]=times[counter].split(";");
		} //end foreach
		
		var timeSelect = document.getElementById("times");
		timeSelect.options[0]=new Option("",""); //first option is blank
		for (counter=0; counter<times.length-1; counter++){ //last element will be empty
			timeSelect.options[counter+1]=new Option(times[counter][0]+" "+times[counter][1], times[counter][3]);
			if (times[counter][2] < 1) {
				timeSelect.options[counter+1].disabled = "disabled";
			} //end if
		} //end for
	} //end function
	);
} //end reportListTimes

function initReport() {
	loadClasses();
	resetReportForm();
} //end initReport

function generateReport() {
	//get list of student schedule to present at time on day
	var times = document.getElementById("times").value;
	information["times"] = {"value": times}; 
	$.post("final-new.php", {"data_to_send":information, "operation":"generate"}, function(response, status, xhr) {
		//if capacity == 0 then disable option
		var names = response.split("\n");
		var reportDiv = document.getElementById("report");
		var report = "<p>"
		
		for (counter=0; counter<names.length-1; counter++){ //last element will be empty
			report = report + names[counter] + "<br />";
		} //end for
		
		report = report + "</p>";
		reportDiv.innerHTML = report;
	} //end function
	);
} //end generateReport
